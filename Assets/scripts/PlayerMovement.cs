﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : Locomotion {

    public enum charTypes {Archer, Warrior};
    public charTypes charType = charTypes.Warrior;

    public LineRenderer directionRenderer;
    public float maxAttackDistance = 5f;
    public GameObject mainCamera;
    public Transform view;

    protected UserInput inputHandler;

    public override void Initialize()
    {
        base.Initialize();
        view = GameObject.FindGameObjectWithTag("ViewDirectionObj").transform;
        speed = 25f;
        inputHandler = Camera.main.GetComponent<UserInput>();
    }

    protected override void UpdateMoveDirection()
    {
        direction = inputHandler.GetMoveDirection();
    }

    public virtual void RotateByBodyRotation()
    {
        if (bodyDirection.magnitude > 0.01f)
        {
            Rotate(bodyDirection, 2f);
        }
    }

    protected override void UpdateBodyDirection () {
        bodyDirection = GetPlayerBodyDirection();
        if (!inputHandler.IsActionInitialized())
        {
            inputHandler.Reset();
            ToIdle();
        }
        else if (!IsAttackPreparing() && !IsAttacking())
        {
            InitAttackPreparing();
        }
    }

    protected virtual Vector3 GetPlayerBodyDirection()
    {
        return inputHandler.GetBodyDirection();
    }

    void InitAttackPreparing()
    {
        ToAttackPreparing();
        inputHandler.OnInitAttackPreparing();
    }

    public override void AttackMovement(float speedLimit)
    {
        // if (target)
        // {
        //     direction = (target.position - transform.position).normalized;
        // }
        CommonMovement(speedLimit);
    }

    protected override void Idle() {
        attackSense.ResetTarget();
        ResetWeapon();
        CommonMovement(maxMovementSpeed);
    }

    protected override void AttackPreparing () {}

    protected override void Attack() {}

    protected virtual IEnumerator ResetWeapon() {
        yield return null;
    }
}

