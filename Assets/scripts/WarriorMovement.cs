﻿using UnityEngine;
using System.Collections;

public class WarriorMovement : PlayerMovement {
    public float maxAttackMoveSpeed = 1f;

    private LayerMask enemyLayerMask;

    void Start()
    {
        base.Initialize();
        enemyLayerMask = LayerMask.GetMask("Enemies");
    }

    protected override void ProcessState()
    {
        base.ProcessState();
    }

    protected override void Attack()
    {
        if (!CheckTarget())
        {
            return;
        }

        if (attackSense.enemy.IsDead())
        {
            ToIdle();
            return;
        }
        RotateByBodyRotation();
        AttackMovement(maxAttackMoveSpeed);
        attackSense.AttackEnemy();
    }

    public bool CheckTarget()
    {
        float angle = Quaternion.Angle(view.rotation, transform.rotation);
        if (angle > 20f)
        {
            return !HandleOtherTargetInView();
        }
        return true;
    }

    public bool HandleOtherTargetInView ()
    {
        RaycastHit hit;
        bool inView = Physics.Raycast(view.position, view.forward, out hit, maxAttackDistance, enemyLayerMask);
        if (attackSense.target && attackSense.target == hit.transform)
        {
            inView = false;
        }
        if (inView)
        {
            if (attackSense.enemiesInRange.Contains(hit.transform))
            {
                attackSense.SetTarget(hit.transform);
            }
        }
        return inView;
    }


    protected override void AttackPreparing()
    {
        if (attackSense.CanAttackTarget())
        {
            ToAttacking();
        }
        else
        {
            if (attackSense.IsTargetInRange())
            {
                if (direction == Vector3.zero)
                {
                    direction = bodyDirection;
                }
            }
            else if (!HandleOtherTargetInView())
            {
                attackSense.OnAttackEnd();
            }
            AttackMovement(maxAttackChasingMoveSpeed);
            RotateByBodyRotation();
        }
    }

    protected override Vector3 GetPlayerBodyDirection()
    {
        Vector3 dir = inputHandler.GetBodyDirection();
        view.rotation = transform.rotation;
        if (dir != Vector3.zero)
        {
            view.rotation = Quaternion.LookRotation(dir);
        }
        if (attackSense.IsTargetInRange())
        {
            dir = attackSense.GetTargetDirection();
        }
        return dir;
    }

    public override void ToIdle()
    {
        attackSense.OnAttackEnd();
        base.ToIdle();
    }
}