﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackSense : MonoBehaviour {

    public float attackDistance = 2f;
    public float attackSpeed = 10f;
    public float attackResetSpeed = 6f;
    public float attackDamage = 40f;
    public float attackForce = 1f;
    public Transform weapon;
    public Transform target;

    private Quaternion startWeaponRotation;
    private Quaternion endWeaponRotation;
    private Quaternion weaponTargetRotation;
    private float weaponRotateSpeed;
    private bool isAttackInProgress = false;
    private bool isResetingWeapon = false;
    public Enemy enemy;

    private SphereCollider col;
    protected Locomotion motorAPI;

    public List<Transform> enemiesInRange = new List<Transform>();

    void Start()
    {
        if (!motorAPI)
        {
            motorAPI = transform.parent.GetComponent<Locomotion>();
        }
        weapon = Utils.FindInChildWithTag(transform.parent.transform, "Weapon").transform;
        startWeaponRotation = weapon.localRotation;
        endWeaponRotation = GetEndWeaponRotation();
        weaponTargetRotation = endWeaponRotation;
        weaponRotateSpeed = attackResetSpeed;
    }

    public virtual void AttackEnemy()
    {
        if (CanAttackTarget())
        {
            AttackByWeapon();
        }
        else
        {
            motorAPI.ToAttackPreparing();
        }
    }

    public virtual void ResetTarget()
    {
        target = null;
    }

    public virtual void SetTarget(Transform t)
    {
        ResetTarget();
        target = t;
    }

    public void OnAttackEnd()
    {
        ResetTarget();
        HandleResetWeapon();
    }

    public void HandleResetWeapon()
    {
        if (!isResetingWeapon && Quaternion.Angle(weapon.localRotation, startWeaponRotation) > 1f)
        {
            StartCoroutine("ResetWeapon");
        }
    }

    protected virtual Quaternion GetEndWeaponRotation()
    {
        return weapon.localRotation * Quaternion.Euler(0, 90, 0);
    }

    void AttackByWeapon()
    {
        if (Quaternion.Angle(weapon.localRotation, weaponTargetRotation) < 5f)
        {
            if (weaponTargetRotation == startWeaponRotation)
            {
                weaponTargetRotation = endWeaponRotation;
                weaponRotateSpeed = attackResetSpeed;
                enemy.ReceiveDamage(attackDamage, attackForce, transform);
            }
            else
            {
                weaponTargetRotation = startWeaponRotation;
                weaponRotateSpeed = attackSpeed;
            }
        }
        weapon.localRotation = Quaternion.Lerp(weapon.localRotation, weaponTargetRotation, Time.deltaTime * weaponRotateSpeed);
    }

    protected virtual IEnumerator ResetWeapon() {
        isResetingWeapon = true;
        while (!motorAPI.IsAttacking())
        {
            weapon.localRotation = Quaternion.Lerp(weapon.localRotation, startWeaponRotation, Time.deltaTime * attackSpeed);
            yield return null;
        }
        isResetingWeapon = false;
    }

    public virtual bool CanAttackTarget()
    {
        return target
               && Vector3.Distance(target.position, transform.position) < attackDistance
               && !enemy.IsDead();
    }

    public virtual bool IsTargetInRange()
    {
        return target && enemiesInRange.Contains(target);
    }

    public virtual Vector3 GetTargetDirection()
    {
        return (target.position - transform.position).normalized;
    }
}
