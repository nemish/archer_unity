using UnityEngine;
using System.Collections;

public class Locomotion : MonoBehaviour {

    public float speed = 15f;
    public float rotationSpeed = 6f;
    public float maxMovementSpeed = 4f;
    public float maxAttackChasingMoveSpeed = 2f;
    public float minStrength = 1f;
    public float rotationAccuracyAngle = 2f;

    public enum states {Idle, AttackPreparing, Attacking}
    public states currentState = states.Idle;

    protected Vector3 direction;
    protected Vector3 bodyDirection;
    protected AttackSense attackSense;

    void Start () {
        Initialize();
    }

    public virtual void Initialize()
    {
        attackSense = Utils.FindInChildWithTag(transform, "AttackSense").GetComponent<AttackSense>();
    }

    void FixedUpdate () {
        ProcessInput();
        ProcessState();
    }

    protected virtual void ProcessState()
    {
        if (IsAttacking())
        {
            Attack();
        }
        else if (IsAttackPreparing())
        {
            AttackPreparing();
        }
        else
        {
            Idle();
        }
    }

    void ProcessInput ()
    {
        UpdateMoveDirection();
        UpdateBodyDirection();
    }

    protected virtual void UpdateMoveDirection () {}

    protected virtual void UpdateBodyDirection () {}

    public bool IsAttacking()
    {
        return CheckState(states.Attacking);
    }

    public bool IsAttackPreparing()
    {
        return CheckState(states.AttackPreparing);
    }

    public bool IsIdle()
    {
        return CheckState(states.Idle);
    }

    public virtual void ToIdle()
    {
        SetState(states.Idle);
    }

    public void ToAttackPreparing()
    {
        SetState(states.AttackPreparing);
    }

    public void ToAttacking()
    {
        SetState(states.Attacking);
    }

    public void SetState(states state)
    {
        currentState = state;
    }

    public bool CheckState(states state)
    {
        return currentState == state;
    }

    public virtual void AttackMovement(float speedLimit) {}

    protected void CommonMovement(float speedLimit)
    {
        if (direction.magnitude > 0)
        {
            Move(speedLimit, direction);
            if (bodyDirection == Vector3.zero)
            {
                Rotate(direction);
            }
        }
    }

    protected void Move(float speedLimit, Vector3 dir)
    {
        if(rigidbody.velocity.magnitude > speedLimit)
        {
           rigidbody.velocity = rigidbody.velocity.normalized * speedLimit;
        } else {
            rigidbody.AddForce(dir * speed);
        }
    }

    protected void Rotate (Vector3 direction, float rotationRatio = 1f) {
        rigidbody.MoveRotation(Quaternion.Lerp(
            rigidbody.rotation, Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z)), rotationSpeed * rotationRatio * Time.deltaTime
        ));
    }

    protected virtual void AttackPreparing () {}

    protected virtual void Attack() {}

    protected virtual void Idle () {
        CommonMovement(maxMovementSpeed);
    }
}
