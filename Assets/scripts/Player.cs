﻿using UnityEngine;
using System.Collections;

public class Player : Character {

    public override void ReceiveDamage(float damage, float attackForce, Transform enemy)
    {
        health -= damage;
        rigidbody.AddForce(
            new Vector3(
                transform.position.x - enemy.position.x, transform.position.y,
                transform.position.z - enemy.position.z
            ) * attackForce, ForceMode.Impulse
        );
    }
}
