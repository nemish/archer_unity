﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttackSense : AttackSense {

    public override void SetTarget(Transform t)
    {
        base.SetTarget(t);
        target.renderer.material.color = Color.blue;
        enemy = target.GetComponent<Enemy>();
    }

    public override void ResetTarget()
    {
        if (target)
        {
            target.renderer.material.color = Color.white;
            enemy = null;
        }

        base.ResetTarget();
    }

    void OnTriggerStay(Collider col){
        if (col.tag == "Enemy")
        {
            if (!enemiesInRange.Contains(col.transform))
            {
                enemiesInRange.Add(col.transform);
            }
            if (!target)
            {
                col.renderer.material.color = Color.red;
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Enemy")
        {
            if (enemiesInRange.Contains(col.transform))
            {
                enemiesInRange.Remove(col.transform);
            }
            col.renderer.material.color = Color.white;
        }
    }
}
