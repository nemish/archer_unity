﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class TouchInput : UserInput
{
    public float maxJoystickDelta = 0.1f;
    public GameObject leftJoy;
    public GameObject rightJoy;

    private struct Coords
    {
        public float x;
        public float y;
    }

    private enum Sides { Left, Right }
    private Dictionary<Sides, int> joyTouches = new Dictionary<Sides, int>();
    private Dictionary<Sides, Vector3> joyTouchesOriginal = new Dictionary<Sides, Vector3>();
    private Dictionary<Sides, GameObject> joysticks = new Dictionary<Sides, GameObject>();

    void Update()
    {
        #if UNITY_IPHONE
        ProcessTouchInput();
        #endif
    }

    protected override void InitializeCurrent() {}

    protected override void InitializeCommon()
    {
        leftJoy = GameObject.FindGameObjectWithTag("LeftJoystick");
        rightJoy = GameObject.FindGameObjectWithTag("RightJoystick");

        joysticks.Add(Sides.Left, leftJoy);
        joysticks.Add(Sides.Right, rightJoy);

        joyTouchesOriginal.Add(Sides.Left, Vector3.zero);
        joyTouchesOriginal.Add(Sides.Right, Vector3.zero);
        base.InitializeCommon();
    }

    public override void Reset()
    {
        leftJoy.SetActive(false);
        rightJoy.SetActive(false);
    }

    public override bool IsActionInitialized()
    {
        return rightJoy.activeSelf;
    }

    public override void OnInitAttackPreparing() {}

    public override Vector3 GetMoveDirection()
    {
        return getTouchMoveDirection();
    }

    public override Vector3 GetBodyDirection()
    {
        return getTouchBodyDirection();
    }

    private Vector3 getTouchMoveDirection()
    {
        return getJoystickDirection(Sides.Left);
    }

    private Vector3 getTouchBodyDirection()
    {
        return getJoystickDirection(Sides.Right);
    }

    private Vector3 getJoystickDirection(Sides side)
    {
        GameObject joystick = joysticks[side];
        if (joystick.activeSelf)
        {
            return new Vector3(
                joystick.transform.position.x - joyTouchesOriginal[side].x, 0,
                joystick.transform.position.y - joyTouchesOriginal[side].y
            ).normalized;
        }
        return Vector3.zero;
    }

    public void ProcessTouchInput()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    OnTouchBegan(touch);
                }
                else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                {
                    OnTouchStay(touch);
                }
                else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    OnTouchEnded(touch);
                }
            }
        }
        else if (joyTouches.Count > 0)
        {
            joyTouches.Clear();
        }
    }

    void OnTouchBegan(Touch touch)
    {
        if (joyTouches.Count <= 2)
        {
            Sides joySide = getJoystickSide(touch);
            if (!joyTouches.ContainsKey(joySide))
            {
                joyTouches.Add(joySide, touch.fingerId);
                Coords coords = getJoyTouchCoords(touch);
                joyTouchesOriginal[joySide] = new Vector3(coords.x, coords.y, 0);
                GameObject joystick = joysticks[joySide];
                joystick.SetActive(true);
                joystick.transform.position = joyTouchesOriginal[joySide];
            }
        }
    }

    void OnTouchStay(Touch touch)
    {
        if (joyTouches.ContainsValue(touch.fingerId))
        {
            Sides joySide = getJoystickSideFromDict(touch);
            Coords coords = getJoyTouchCoords(touch);
            joysticks[joySide].transform.position = Vector3.ClampMagnitude(new Vector3(
                coords.x - joyTouchesOriginal[joySide].x, coords.y - joyTouchesOriginal[joySide].y, 0
            ), maxJoystickDelta) + joyTouchesOriginal[joySide];
        }
    }

    void OnTouchEnded(Touch touch)
    {
        Sides joySide = getJoystickSideFromDict(touch);
        joyTouches.Remove(joySide);
        joyTouchesOriginal[joySide] = Vector3.zero;
        joysticks[joySide].SetActive(false);
    }

    Sides getJoystickSideFromDict(Touch touch)
    {
        var keys = from record in joyTouches
                where record.Value == touch.fingerId
                select record.Key;
        return keys.First();
    }

    Sides getJoystickSide(Touch touch)
    {
        return touch.position.x < (Screen.width / 2) ? Sides.Left : Sides.Right;
    }

    Coords getJoyTouchCoords(Touch touch)
    {
        return new Coords { x = touch.position.x / Screen.width, y = touch.position.y / Screen.height };
    }
}