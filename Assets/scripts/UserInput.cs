﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UserInput : MonoBehaviour
{
    public float mouseAimingDeltaLimit = 1f;
    public LayerMask mouseAimingPlaneLM;
    public Vector3 mouseHoldBeginPoint;
    public Vector3 mouseHoldCurrentPoint;
    public Vector3 mouseHoldBeginPosition;
    public Vector3 mouseHoldCurrentPosition;
    public GUIText score;

    protected WarriorMovement playerAPI;

    void Start()
    {
        InitializeCommon();
        InitializeCurrent();
    }

    protected virtual void InitializeCommon()
    {
        mouseAimingPlaneLM = LayerMask.GetMask("MouseAimingPlane");
        Reset();
        if (!playerAPI)
        {
            playerAPI = GameObject.FindGameObjectWithTag("Player").GetComponent<WarriorMovement>();
        }
    }

    protected virtual void InitializeCurrent()
    {
        StartCoroutine("DrawDirection", GameObject.FindGameObjectWithTag("DirectionArrow").GetComponent<LineRenderer>());
    }

    public virtual void Reset()
    {
        mouseHoldBeginPoint = Vector3.zero;
        mouseHoldCurrentPoint = Vector3.zero;
        mouseHoldBeginPosition = Vector3.zero;
        mouseHoldCurrentPosition = Vector3.zero;
    }

    public virtual void OnInitAttackPreparing()
    {
        CheckBeginPosition();
    }

    public void CheckBeginPosition()
    {
        mouseHoldBeginPoint = GetBodyInputValue();
        mouseHoldBeginPosition = Input.mousePosition;
    }

    public void CheckCurrentPosition()
    {
        mouseHoldCurrentPoint = GetBodyInputValue();
        mouseHoldCurrentPosition = Input.mousePosition;
    }

    public Vector3 GetDirectionLineBeginPoint()
    {
        return getDirectionLinePoint(mouseHoldBeginPoint);
    }

    public Vector3 GetDirectionLineCurrentPoint()
    {
        return getDirectionLinePoint(mouseHoldCurrentPoint);
    }

    private Vector3 getDirectionLinePoint(Vector3 mousePosition)
    {
        Vector3 point = Vector3.zero;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(mousePosition.x, mousePosition.z, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, mouseAimingPlaneLM))
        {
            point = hit.point;
        }
        return point;
    }

    public virtual bool IsActionInitialized()
    {
        return mouseHoldCurrentPoint != Vector3.zero;
    }

    public virtual Vector3 GetMoveDirection()
    {
        return new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
    }

    public virtual Vector3 GetBodyDirection()
    {
        Vector3 bodyDirection = Vector3.zero;
        if (Input.GetButton("Fire1")) {
            CheckCurrentPosition();
            bodyDirection = calcBodyDirection(mouseHoldCurrentPoint, mouseHoldBeginPoint);
        }
        else
        {
            Reset();
        }
        return bodyDirection;
    }

    protected Vector3 calcBodyDirection (Vector3 beginPoint, Vector3 currentPoint)
    {
        return Vector3.zero + (beginPoint - currentPoint);
    }

    Vector3 GetBodyInputValue () {
        return new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y);
    }

    IEnumerator DrawDirection(LineRenderer line) {
        Vector3 beginPos = Vector3.zero;
        Vector3 currentPos = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 cameraOffsetBegin = Vector3.zero;
        while (true)
        {
            if (mouseHoldCurrentPosition != Vector3.zero)
            {
                if (beginPos == Vector3.zero)
                {
                    beginPos = GetDirectionLineBeginPoint();
                    cameraOffsetBegin = transform.position - beginPos;
                }
                currentPos = GetDirectionLineCurrentPoint();
                direction = currentPos - beginPos;
                if (currentPos != Vector3.zero && direction.magnitude > mouseAimingDeltaLimit)
                {
                    currentPos = beginPos + Vector3.ClampMagnitude(direction, mouseAimingDeltaLimit);
                }
                beginPos = transform.position - cameraOffsetBegin;
            }
            else
            {
                beginPos = Vector3.zero;
                currentPos = Vector3.zero;
            }

            if (currentPos == Vector3.zero || beginPos == Vector3.zero)
            {
                beginPos = Vector3.zero;
                currentPos = Vector3.zero;
            }

            line.SetPosition(0, beginPos);
            line.SetPosition(1, currentPos);
            yield return null;
        }
    }
}
