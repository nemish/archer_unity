﻿using UnityEngine;
using System.Collections;

public class UserGUI : MonoBehaviour {

    public GUIText score;
    public int enemiesKilled = 0;

    void Start()
    {
        GameObject.FindGameObjectWithTag("LeftJoystick").SetActive(false);
        GameObject.FindGameObjectWithTag("RightJoystick").SetActive(false);
        score = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<GUIText>();
        score.text = "Score: " + enemiesKilled.ToString();
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 100, 20), "Начать заново"))
        {
            Application.LoadLevel("Scene_warrior");
        }
    }

    public void UpdateScore()
    {
        ++enemiesKilled;
        score.text = "Score: " + enemiesKilled.ToString();
    }

}
