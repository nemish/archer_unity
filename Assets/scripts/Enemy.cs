﻿using UnityEngine;
using System.Collections;

public class Enemy : Character {
    private Transform enTextCont;
    private TextMesh enText;

    protected override void Initialize()
    {
        base.Initialize();
        enTextCont = transform.Find("EnemyTextContainer");
        enText = enTextCont.GetComponent<TextMesh>();
    }

	protected override void Update () {
        base.Update();
        enTextCont.forward = Camera.main.transform.forward;
	}

    public override void ReceiveDamage(float damage, float attackForce, Transform player)
    {
        health -= damage;
        rigidbody.AddForce(
            new Vector3(
                transform.position.x - player.position.x, transform.position.y,
                transform.position.z - player.position.z
            ) * attackForce, ForceMode.Impulse
        );
        UpdateEnemyText();
    }

    void UpdateEnemyText()
    {
        enText.text = string.Format("{0:0}", health);
    }

    protected override void Dead()
    {

        base.Dead();
        gui.UpdateScore();
        Destroy(gameObject);
    }
}
