﻿using UnityEngine;
using System.Collections;

public class EnemyAttackSense : AttackSense {

    void OnTriggerStay(Collider col){
        if (col.tag == "Player")
        {
            SetTarget(col.transform);
            motorAPI.ToAttackPreparing();
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            ResetTarget();
        }
    }
}
