﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    public float health = 100f;

    protected UserGUI gui;
    protected bool dead = false;

    void Start()
    {
        Initialize();
    }

    protected virtual void Initialize()
    {
        gui = GameObject.FindGameObjectWithTag("UserGUI").GetComponent<UserGUI>();
    }

    protected virtual void Update () {
        if (health < 0f)
        {
            Dead();
        }
    }

    public virtual void ReceiveDamage(float damage, float attackForce, Transform player)
    {
    }

    public bool IsDead()
    {
        return dead;
    }

    protected virtual void Dead()
    {
        dead = true;
    }

}
