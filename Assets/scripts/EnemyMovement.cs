﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : Locomotion {

    public float attackDistance = 2f;

    protected override void ProcessState()
    {
        base.ProcessState();
    }

    protected override void UpdateMoveDirection()
    {
        if (attackSense.target)
        {
            direction = (attackSense.target.position - transform.position).normalized;
        }
        else
        {
            direction = Vector3.zero;
        }
    }

    protected override void AttackPreparing()
    {
        if (attackSense.target)
        {
            if (Vector3.Distance(attackSense.target.position, transform.position) < attackDistance)
            {
                ToAttacking();
            }
            else
            {
                AttackMovement(maxAttackChasingMoveSpeed);
            }
        }
    }
}
