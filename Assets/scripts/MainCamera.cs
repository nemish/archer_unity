﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {

    private Transform player;
    private Vector3 offset;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[0].transform;
        offset = transform.position - player.position;
	}

	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(player.position.x + offset.x, transform.position.y, player.position.z + offset.z);
	}
}
