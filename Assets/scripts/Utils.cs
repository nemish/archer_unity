﻿using UnityEngine;
using System.Collections;

public class Utils
{

    public static GameObject FindInChildWithTag(Transform t, string tag)
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag(tag))
        {
            if (obj.transform.parent == t)
            {
                return obj;
            }
        }
        return null;
    }

}
